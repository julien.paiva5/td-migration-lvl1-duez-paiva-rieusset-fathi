CREATE TABLE IF NOT EXISTS utilisateurs
(
	nom varchar(255),
	prenom varchar(255),
	email varchar(255),
	motdepasse varchar(255)
);

ALTER TABLE IF EXISTS utilisateurs
    OWNER to postgres;
